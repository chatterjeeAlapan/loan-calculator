import React, { Component } from 'react';

import Layout from './hoc/Layout/Layout';
import Modal from './components/UI/Modal/Modal';
import LoanCalculator from './containers/LoanCalculator/LoanCalculator';
import Spinner from './components/UI/Spinner/Spinner';
import axios from './axios-loan';
import withErrorHandler from './hoc/withErrorHandler/withErrorHandler';
import LoanContext from './context/loan-context';

function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loanDetails : {
        amount: 500,
        duration: 6,
        interestRate: 0,
        monthlyPayment: 0
      },
      loanIsValid: false,
      loading: false,
      showModal: false
    };
    this.calculateLoanPayments = debounce(this.calculateLoanPayments.bind(this), 1000);
  }

  componentDidUpdate(prevProps, prevState) {
    if(prevState.loanDetails.amount !== this.state.loanDetails.amount || prevState.loanDetails.duration !== this.state.loanDetails.duration){
      this.calculateLoanPayments();
    }
  }

  calculateLoanPayments() {
    this.setState({loading: true});
    const queryString = `interest?amount=${encodeURIComponent(this.state.loanDetails.amount)}&numMonths=${encodeURIComponent(this.state.loanDetails.duration)}`;
      axios.get(queryString).then(response => {
        const interestRate    = response.data.interestRate;
        const monthlyPayment  = response.data.monthlyPayment.amount;
        const updatedLoanDetails = {
          ...this.state.loanDetails,
          interestRate,
          monthlyPayment
        };
        this.setState({
          loanDetails: updatedLoanDetails,
          loading: false,
          loanIsValid: true
        });
      }).catch(error => {
        this.setState({loading: false});
      })
  }

  inputChangedHandler = (value, loanDetailsIdentifier) => {
    if(loanDetailsIdentifier === 'amount' || loanDetailsIdentifier === 'duration'){
      const updatedLoanDetails = {...this.state.loanDetails};
      updatedLoanDetails[loanDetailsIdentifier] = value;
      this.setState({
        loanDetails: updatedLoanDetails
      });
    }
  }

  loanDetailsSavedHandler = () => {
    let loanDetails = localStorage.getItem("loandetails-storage");
    let details  = {
      loanDetails: this.state.loanDetails,
      savedAt: +new Date()
    };
    if(!loanDetails){
      localStorage.setItem("loandetails-storage", JSON.stringify([details]));
    }else{
      let detailsArr = JSON.parse(loanDetails);
      detailsArr.push(details);
      localStorage.setItem("loandetails-storage", JSON.stringify(detailsArr));
    }
    this.setState({showModal: true});
  }

  loanItemSelectedHandler = (amount, period) => {
    const loanDetails = {
      ...this.state.loanDetails,
      amount,
      period
    };
    this.setState({
      loanDetails: loanDetails
    });
  }

  modalDismissedHandler = () => {
    this.setState({showModal: false});
  }

  render() {
    let element = (
      <LoanCalculator 
        {...this.state.loanDetails}
        changed={this.inputChangedHandler} 
        calculate={this.calculateLoanPayments}
        saved={this.loanDetailsSavedHandler} 
        invalid={!this.state.loanIsValid} />
    );
    
    if(this.state.loading){
      element = <Spinner />;
    }

    return (
      <div>
        <LoanContext.Provider value={{
            select: this.loanItemSelectedHandler
          }}>
            <Layout>
              <Modal show={this.state.showModal} modalClosed={this.modalDismissedHandler}>
                  <p>Loan Saved Successfully!</p>
              </Modal>
                {element}
            </Layout>
        </LoanContext.Provider>
      </div>
    );
  }
}

export default withErrorHandler(App, axios);
