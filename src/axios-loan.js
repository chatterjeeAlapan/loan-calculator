import axios from 'axios';

const instance = axios.create({
    baseURL: "https://ftl-frontend-test.herokuapp.com/"
});

export default instance;