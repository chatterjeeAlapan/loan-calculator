import React, {Component} from 'react';

import Input from '../../components/UI/Input/Input';
import Button from '../../components/UI/Button/Button';
import classes from './LoanCalculator.css';

const slideramountConfig   = {min: 500, max: 5000};
const sliderDurationConfig = {min: 6, max: 24};

class LoanCalculator extends Component {

    constructor(props){
        super(props);
        this.state = {
            loanData : {
                amount: {
                    elementType: 'slider',
                    elementConfig: slideramountConfig,
                    validation: {},
                    valid: true,
                    label: "Amount($)"
                },
                duration: {
                    elementType: 'slider',
                    elementConfig: sliderDurationConfig,
                    validation: { },
                    valid: true,
                    label: "Duration(Months)"
                },
                interestRate: {
                    elementType: 'input',
                    elementConfig: {
                        type: 'text',
                        placeholder: 'Rate of Interest',
                        disabled: true
                    },
                    validation: { },
                    valid: true,
                    label: "Rate of Interest"
                },
                monthlyPayment: {
                    elementType: 'input',
                    elementConfig: {
                        type: 'text',
                        placeholder: 'Monthly payment',
                        disabled: true
                    },
                    validation: { },
                    valid: true,
                    label: "Monthly Payment($)"
                }
            }
        };
    }

    inputChangedHandler(event, formIdentifier){
        this.props.changed(event.target.value, formIdentifier);
    }

    loanCalculateHandler = (event) => {
        event.preventDefault();
        this.props.calculate();
    }

    loanSavedHandler = (event) => {
        event.preventDefault();
        this.props.saved();
    }

    render() {

        const loanFormElements = Object.keys(this.state.loanData).map(elKey => {
            const formEl = this.state.loanData[elKey];
            return <Input 
                key={elKey}
                elementType={formEl.elementType}
                elementConfig={formEl.elementConfig}
                value={this.props[elKey]}
                label={formEl.label}
                changed={(event) => this.inputChangedHandler(event, elKey)} />;
        });

        return <div className={classes.LoanCalculator}>
            <h4>Enter loan details below</h4>
            <form style={{textAlign: 'left'}}>
                {loanFormElements}
                <div style={{textAlign: 'center'}}>
                    <Button btnType="Danger" clicked={this.loanCalculateHandler}>CALCULATE</Button>
                    <Button btnType="Success" clicked={this.loanSavedHandler} disabled={this.props.invalid}>SAVE</Button>
                </div>
            </form>
        </div>;
    }
}

export default LoanCalculator;