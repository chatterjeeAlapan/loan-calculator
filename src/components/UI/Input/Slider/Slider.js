import React from 'react';

import classes from './Slider.css';

const slider = props => {
    return (
        <div className={classes.SliderContainer}>
            <input 
                type="range"
                {...props.elementConfig} 
                value={props.value} 
                className={classes.Slider}
                onChange={props.changed} />
            <p>Value: <strong>{props.value}</strong></p>
        </div>
    );
}

export default slider;