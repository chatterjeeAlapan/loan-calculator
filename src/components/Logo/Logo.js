import React from 'react';

import calculatorLogo from '../../assets/images/calculator-logo.png';
import classes from './Logo.css';

const logo = props => {
    return <div className={classes.Logo} style={{...props.style, 'height': props.height}}>
        <img src={calculatorLogo} alt="Loan Calculator" />
    </div>;
};

export default logo;