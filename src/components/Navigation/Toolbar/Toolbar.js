import React from 'react';

import DrawerToggle from '../SideDrawer/DrawerToggle/DrawerToggle';
import Logo from '../../Logo/Logo';
import classes from './Toolbar.css';

const styleObject = {
    margin: 0,
    display: 'flex',
    height: '100%',
    width: 'auto',
    textAlign: 'center',
    color: 'white',
    padding:  '16px 10px',
    borderBottom: '4px solid #40A4C8',
    boxSizing: 'border-box'
}

const toolbar = props => {
    return (
        <header className={classes.Toolbar}>
            <DrawerToggle clicked={props.drawerToggleClicked} />
            <Logo height="90%" />
            <div className={classes.DesktopOnly}>
                <p style={styleObject}>LOAN CALCULATOR</p>
            </div>
        </header>
    );
}

export default toolbar;