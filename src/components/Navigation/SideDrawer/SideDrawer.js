import React from 'react';

import LoanItems from '../../LoanItems/LoanItems';
import Backdrop from '../../UI/Backdrop/Backdrop';
import Aux from '../../../hoc/Auxiliary/Auxiliary';
import classes from './SideDrawer.css';

const sideDrawer = props => {
    return (
        <Aux>
            <Backdrop show={props.isOpen} clicked={props.closed}/>
            <div className={[classes.SideDrawer, props.isOpen ? classes.Open : classes.Close].join(' ')}>
                <p>Saved Items:</p>
                {
                    props.isOpen ? 
                    <nav>
                        <LoanItems />
                    </nav> 
                    : null
                }
            </div>
        </Aux>
    );
};

export default sideDrawer;