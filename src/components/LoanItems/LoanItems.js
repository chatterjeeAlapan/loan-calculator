import React, {Component} from 'react';

import LoanItem from './LoanItem/LoanItem';
import classes from './LoanItems.css';

class LoanItems extends Component {

    constructor(props){
        super(props);
        this.state = {
            loanItems: null
        };
    }
    
    componentDidMount() {
        let loanDetails = JSON.parse(localStorage.getItem("loandetails-storage"));
        if(loanDetails){
            loanDetails = loanDetails.reverse();
        }
        this.setState({
            loanItems: loanDetails
        });
    }

    render() {
        let loanItems = <p>No Items Saved!</p>;
        if(this.state.loanItems){
            loanItems = (
                <ul className={classes.LoanItems}>
                    {
                        this.state.loanItems.map((item, i) => {
                            return <LoanItem 
                                key={i+1}
                                amount={item.loanDetails.amount}
                                duration={item.loanDetails.duration}
                                ts={item.loanDetails.savedAt} />
                        })
                    }
                </ul>
            );
        }
        return loanItems;
    }
    
};

export default LoanItems;