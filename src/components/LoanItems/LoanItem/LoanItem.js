import React, {useContext} from 'react';

import LoanContext from '../../../context/loan-context';
import classes from './LoanItem.css';

const loanItem = props => {

    const context = useContext(LoanContext);

    return <li className={classes.LoanItem}>
        <a onClick={(event) => {
            context.select(props.amount, props.duration);
        }}>
            {`$ ${props.amount} / ${props.duration} Months`}
        </a>
    </li>
};

export default loanItem;